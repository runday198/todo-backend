import mongoose from "mongoose";
import { hashPass } from "../utils/bcryptUtils.js";
import TaskSchema from "./task.js";

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  tasks: [TaskSchema],
});

UserSchema.pre("save", function (next) {
  const user = this;

  if (!user.isModified("password")) {
    return next();
  }

  return hashPass(user.password)
    .then((hashedPass) => {
      user.password = hashedPass;
      next();
    })
    .catch((err) => {
      next(err);
    });
});

UserSchema.methods.getTask = function getTask(title) {
  const task = this.tasks.find((item) => item.title === title);
  return task;
};

UserSchema.methods.getDoneTasks = function getDoneTasks() {
  const tasks = this.tasks.filter((item) => item.done === true);
  return tasks;
};

const UserModel = mongoose.model("User", UserSchema);

export default UserModel;
