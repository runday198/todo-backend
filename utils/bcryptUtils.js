import bcrypt from "bcrypt";

export async function checkPassword(password, hashedPassword) {
  try {
    const hasMatched = await bcrypt.compare(password, hashedPassword);
    if (!hasMatched) {
      return false;
    }

    return true;
  } catch (err) {
    return false;
  }
}

export async function hashPass(password) {
  try {
    const hashedPass = await bcrypt.hash(password, 12);

    return hashedPass;
  } catch (err) {
    throw err;
  }
}
