import jwt from "jsonwebtoken";

export function productJWTToken(email) {
  return jwt.sign({ email: email }, "Secret Key", { expiresIn: "2h" });
}
