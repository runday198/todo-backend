import bunyan from "bunyan";

const log = bunyan.createLogger({ name: "test", level: "debug" });

export default log;
