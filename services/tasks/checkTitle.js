export async function checkTitle(user, taskTitle) {
  try {
    const tasks = user.tasks;

    let taskIndex = tasks.findIndex((task) => task.title === taskTitle);

    if (taskIndex !== -1) {
      return false;
    }

    return true;
  } catch (err) {
    throw err;
  }
}
