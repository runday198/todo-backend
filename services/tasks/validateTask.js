import taskSchema from "../../validators/taskSchema.js";
import { checkTitle } from "./checkTitle.js";

export async function validateTask(title, description, user) {
  const { error } = taskSchema.validate({ title, description });

  if (error || !(await checkTitle(user, title.trim()))) {
    return false;
  }

  return true;
}
