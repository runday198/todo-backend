import Error404 from "../error/404Error.js";
import UserModel from "../models/user.js";

class UserService {
  static async getOne(email) {
    return UserModel.findOne({ email });
  }

  static async register(data) {
    const user = new UserModel(data);
    return user.save();
  }

  static async addTask(user, data) {
    user.tasks.push(data);
    return user.save();
  }

  static async markDone(user, title) {
    try {
      const task = user.getTask(title);
      if (!task) {
        throw new Error404();
      }

      task.done = true;
      return user.save();
    } catch (err) {
      throw err;
    }
  }

  static async updateTask(user, title, data) {
    try {
      const task = user.getTask(title);
      if (!task) {
        throw new Error404();
      }
      task.title = data.title;
      task.description = data.description;

      return user.save();
    } catch (err) {
      throw err;
    }
  }

  static async deleteTask(user, title) {
    user.tasks = user.tasks.filter((item) => item.title !== title);
    return user.save();
  }
}

export default UserService;
