import mongoose from "mongoose";

async function connectToMongoDB() {
  return mongoose.connect("mongodb://localhost:37017/todo");
}

export default connectToMongoDB;
