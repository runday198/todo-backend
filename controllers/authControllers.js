import userSchema from "../validators/userSchema.js";
import { checkPassword } from "../utils/bcryptUtils.js";
import { productJWTToken } from "../utils/jwtUtils.js";

import Error400 from "../error/400Error.js";
import { catchError } from "../middleware/catchError.js";

import UserService from "../services/UserService.js";

export async function postRegister(req, res, next) {
  const { email, password } = req.body;

  try {
    const { error } = userSchema.validate({ email, password });

    if (error || (await UserService.getOne(email))) {
      throw new Error400();
    }

    await UserService.register({ email, password });

    return res.status(200).end();
  } catch (err) {
    catchError(err, next);
  }
}

export async function postLogin(req, res, next) {
  const { email, password } = req.body;

  try {
    const user = await UserService.getOne(email);
    if (!user || !(await checkPassword(password, user.password))) {
      throw new Error400();
    }

    const token = productJWTToken(email);

    return res.status(200).json({ user: { email }, token });
  } catch (err) {
    catchError(err, next);
  }
}
