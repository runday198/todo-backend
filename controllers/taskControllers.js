import { validateTask } from "../services/tasks/validateTask.js";

import Error400 from "../error/400Error.js";

import { catchError } from "../middleware/catchError.js";
import UserService from "../services/UserService.js";

export async function postAddTask(req, res, next) {
  const { title, description } = req.body;
  const { user } = req;

  try {
    if (!(await validateTask(title, description, user))) {
      throw new Error400();
    }

    await UserService.addTask(user, { title: title.trim(), description });

    return res.status(201).end();
  } catch (err) {
    catchError(err, next);
  }
}

export async function getTasks(req, res, next) {
  const { user } = req;

  try {
    const tasks = user.tasks;

    return res.status(200).json(tasks);
  } catch (err) {
    catchError(err, next);
  }
}

export async function postDone(req, res, next) {
  const { title } = req.body;
  const { user } = req;

  try {
    await UserService.markDone(user, title.trim());

    return res.status(200).end();
  } catch (err) {
    catchError(err, next);
  }
}

export async function getDone(req, res, next) {
  const { user } = req;

  try {
    const tasks = user.getDoneTasks();

    return res.status(200).json(tasks);
  } catch (err) {
    catchError(err, next);
  }
}

export async function updateTask(req, res, next) {
  const { user } = req;
  const { title, description } = req.body;
  const { taskTitle: currentTitle } = req.params;

  try {
    if (
      title.trim() !== currentTitle.trim() &&
      !(await validateTask(title, description, user))
    ) {
      throw new Error400();
    }

    await UserService.updateTask(user, currentTitle.trim(), {
      title: title.trim(),
      description,
    });

    return res.status(200).end();
  } catch (err) {
    catchError(err, next);
  }
}

export async function deleteTask(req, res, next) {
  const { user } = req;
  const { title } = req.body;

  try {
    if (!title) {
      throw new Error400();
    }

    await UserService.deleteTask(user, title.trim());

    return res.status(204).end();
  } catch (err) {
    catchError(err, next);
  }
}
