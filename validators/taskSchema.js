import Joi from "joi";

const taskSchema = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
}).with("title", "description");

export default taskSchema;
