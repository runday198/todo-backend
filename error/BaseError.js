class BaseError extends Error {
  constructor(name, statusCode, isFatal, description) {
    super(description);

    Object.setPrototypeOf(this, new.target.prototype);
    this.name = name;
    this.statusCode = statusCode;
    this.isFatal = isFatal;

    Error.captureStackTrace(this);
  }
}

export default BaseError;
