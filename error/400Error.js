import BaseError from "./BaseError.js";

class Error400 extends BaseError {
  constructor(
    name,
    statusCode = 400,
    isFatal = false,
    description = "Bad Request"
  ) {
    super(name, statusCode, isFatal, description);
  }
}

export default Error400;
