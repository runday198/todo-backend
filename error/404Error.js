import BaseError from "./BaseError.js";

class Error404 extends BaseError {
  constructor(
    name,
    statusCode = 404,
    isFatal = false,
    description = "Not Found"
  ) {
    super(name, statusCode, isFatal, description);
  }
}

export default Error404;
