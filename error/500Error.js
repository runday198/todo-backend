import BaseError from "./BaseError.js";

class Error500 extends BaseError {
  constructor(
    name,
    statusCode = 500,
    isFatal = true,
    description = "Internal Server Error"
  ) {
    super(name, statusCode, isFatal, description);
  }
}

export default Error500;
