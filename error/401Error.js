import BaseError from "./BaseError.js";

class Error401 extends BaseError {
  constructor(
    name,
    statusCode = 401,
    isFatal = false,
    description = "Unauthorized"
  ) {
    super(name, statusCode, isFatal, description);
  }
}

export default Error401;
