import Error500 from "../error/500Error.js";
import BaseError from "../error/BaseError.js";

export function catchError(error, next) {
  if (error instanceof BaseError) {
    return next(error);
  }

  return next(new Error500(error));
}
