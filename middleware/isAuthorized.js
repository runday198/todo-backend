import jwt from "jsonwebtoken";

import Error401 from "../error/401Error.js";
import { catchError } from "./catchError.js";

import UserService from "../services/UserService.js";

export async function isAuthorized(req, res, next) {
  try {
    let authHeader = req.get("Authorization");
    if (!authHeader) {
      throw new Error401();
    }

    let token = authHeader.split(" ")[1];
    if (!token) {
      throw new Error401();
    }

    let payload = jwt.verify(token, "Secret Key");
    let userEmail = payload.email;

    let user = await UserService.getOne(userEmail);
    if (!user) {
      throw new Error401();
    }

    req.user = user;

    next();
  } catch (err) {
    catchError(new Error401(), next);
  }
}
