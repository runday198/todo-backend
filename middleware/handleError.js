import log from "../utils/logger.js";

export async function handleError(err, req, res, next) {
  if (err.isFatal || err.statusCode === 500) {
    log.fatal(err);
    return res.status(err.statusCode || 500).send(err.message);
  }

  log.info(err);
  return res.status(err.statusCode).send(err.message);
}
