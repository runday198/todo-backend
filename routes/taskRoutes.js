import express from "express";
import { isAuthorized } from "../middleware/isAuthorized.js";
import * as taskControllers from "../controllers/taskControllers.js";

const router = express.Router();

router.post("/api/tasks", isAuthorized, taskControllers.postAddTask);

router.get("/api/tasks", isAuthorized, taskControllers.getTasks);

router.post("/api/tasks/done", isAuthorized, taskControllers.postDone);

router.get("/api/tasks/done", isAuthorized, taskControllers.getDone);

router.put("/api/tasks/:taskTitle", isAuthorized, taskControllers.updateTask);

router.delete("/api/tasks/", isAuthorized, taskControllers.deleteTask);

export default router;
