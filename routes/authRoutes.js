import express from "express";
import * as authControllers from "../controllers/authControllers.js";

const router = express.Router();

router.post("/api/auth/register", authControllers.postRegister);

router.post("/api/auth/login", authControllers.postLogin);

export default router;
