import express from "express";
import morgan from "morgan";

import * as authRoutes from "./routes/authRoutes.js";
import * as taskRoutes from "./routes/taskRoutes.js";
import { handleError } from "./middleware/handleError.js";
import connectToMongoDB from "./db/database.js";
import log from "./utils/logger.js";

const app = express();

app.use(express.json());

app.use(morgan("dev"));

app.use(authRoutes.default);
app.use(taskRoutes.default);

app.use(handleError);

connectToMongoDB()
  .then(() => {
    app.listen(3000, () => {
      console.log(`App is running on Port 3000`);
    });
  })
  .catch((error) => {
    log.info("Error connecting to MongoDB:", error);
  });
